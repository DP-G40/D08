package domain;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class User extends Actor {

    // Atributos ----

    private Date birthday;
    // Constructor ----

    public User() {
        super();
    }

    @NotNull
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    //Relaciones

    private Collection<Rendezvous> rendezvous = new ArrayList<Rendezvous>();
    private Collection<Rendezvous> reservations = new ArrayList<Rendezvous>();
    private Collection<Comment> comments = new ArrayList<Comment>();
    private Collection<Announcement> announcements = new ArrayList<Announcement>();

    @NotNull
    @OneToMany(mappedBy = "author")
    public Collection<Rendezvous> getRendezvous() {
        return rendezvous;
    }

    public void setRendezvous(Collection<Rendezvous> rendezvous) {
        this.rendezvous = rendezvous;
    }

    @NotNull
    @ManyToMany(mappedBy = "users")
    public Collection<Rendezvous> getReservations() {
        return reservations;
    }

    public void setReservations(Collection<Rendezvous> reservations) {
        this.reservations = reservations;
    }

    @NotNull
    @OneToMany(mappedBy = "user")
    public Collection<Comment> getComments() {
        return comments;
    }

    public void setComments(Collection<Comment> comments) {
        this.comments = comments;
    }

    @NotNull
    @OneToMany(mappedBy = "user")
    public Collection<Announcement> getAnnouncements() {
        return announcements;
    }

    public void setAnnouncements(Collection<Announcement> announcements) {
        this.announcements = announcements;
    }
}

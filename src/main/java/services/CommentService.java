package services;

import domain.Comment;
import domain.User;
import forms.CommentForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import repositories.CommentRepository;

import java.util.Collection;
import java.util.Date;

@Service
@Transactional
public class CommentService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private ActorService actorService;

    @Autowired
    private RendezvousService rendezvousService;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public CommentService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Comment create(Integer rendezvousId) {
        Comment result;

        result = new Comment();

        result.setMoment(new Date());
        User us = (User) actorService.getPrincipal();
        result.setUser(us);
        result.setRendezvous(rendezvousService.findOne(rendezvousId));


        return result;
    }

    public Collection<Comment> findAll() {
        Collection<Comment> result;
        Assert.notNull(commentRepository);
        result = commentRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Comment findOne(int commentId) {
        Comment result;
        result = commentRepository.findOne(commentId);
        return result;
    }

    public Collection<Comment> getCommentsByUser(Integer userId) {
        return commentRepository.getCommentsByUser(userId);
    }

    public Comment save(Comment comment) {
        assert comment != null;
        Comment result;
        result = commentRepository.save(comment);
        return result;
    }


    public Comment reconstruct(CommentForm commentForm ) {
        Comment result;

        result = this.create(commentForm.getRendezvousId());

        result.setPicture(commentForm.getPicture());
        result.setText(commentForm.getText());


        return result;
    }

    public void delete(Comment comment) {
        Assert.notNull(comment);
        commentRepository.delete(comment);
    }

// Other business methods -------------------------------------------------

    public void deleteAll(Collection<Comment> comments) {
        assert comments != null;
        assert comments.size() != 0;

        for (Comment c : comments) {
            commentRepository.delete(c);
        }
    }
}

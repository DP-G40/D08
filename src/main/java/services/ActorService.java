package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ActorRepository;
import security.LoginService;
import domain.Actor;
import forms.ActorForm;

@Service
@Transactional
public class ActorService {
	// Managed repository -----------------------------------------------------
	@Autowired
	private ActorRepository actorRepository;

	// Suporting repository --------------------------------------------------

	// Constructors -----------------------------------------------------------
	public ActorService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------

	public Collection<Actor> findAll() {
		Collection<Actor> result;
		Assert.notNull(actorRepository);
		result = actorRepository.findAll();
		Assert.notNull(result);
		return result;
	}

	public Actor findOne(int actorId) {
		Actor result;
		result = actorRepository.findOne(actorId);
		return result;
	}

	public Actor save(Actor actor) {
		assert actor != null;
		Actor result;
		result = actorRepository.save(actor);
		return result;
	}

	public Actor getPrincipal() {

		Actor result = actorRepository.getPrincipal(LoginService.getPrincipal()
				.getId());
		Assert.notNull(result);

		return result;
	}

	public Actor save(final ActorForm actorform) {
		Actor result = this.getPrincipal();
		Assert.notNull(result);

		result.setEmail(actorform.getEmail());
		result.setName(actorform.getName());
		result.setPhoneNumber(actorform.getPhoneNumber());
		result.setSurname(actorform.getSurname());
		result.setPhoneNumber(result.getPhoneNumber());

		Assert.notNull(result);
		return result;

	}

	public void delete(Actor actor) {
		assert actor != null;
		assert actor.getId() != 0;
		actorRepository.delete(actor);
	}

	// Other business methods -------------------------------------------------


	public Actor getByUserName(String username){
		return actorRepository.getByUserName(username);
	}
}

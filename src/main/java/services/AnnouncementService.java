package services;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.AnnouncementRepository;
import domain.Announcement;
import domain.User;
import forms.AnnouncementForm;

@Service
@Transactional
public class AnnouncementService {
// Managed repository -----------------------------------------------------
@Autowired
private AnnouncementRepository announcementRepository;

@Autowired
private ActorService actorService;

@Autowired
private RendezvousService rendezvousService;
// Suporting repository --------------------------------------------------

// Constructors -----------------------------------------------------------
public AnnouncementService() {
super();
}
// Simple CRUD methods ----------------------------------------------------
public Announcement create(Integer rendezvousId) {
Announcement result;
result = new Announcement();

User user = (User) actorService.getPrincipal();
result.setMoment(new Date());
result.setRendezvous(rendezvousService.findOne(rendezvousId));
result.setUser(user);

return result;
}

public Collection<Announcement> findAll() {
Collection<Announcement> result;
Assert.notNull(announcementRepository);
result = announcementRepository.findAll();
Assert.notNull(result);
return result;
}

public Announcement findOne(int announcementId) {
Announcement result;
result = announcementRepository.findOne(announcementId);
return result;
}

public Announcement save(Announcement announcement) {
assert announcement != null;
Announcement result;
result = announcementRepository.save(announcement);
return result;
}

public void delete(Announcement announcement) {
assert announcement != null;
assert announcement.getId() != 0;
announcementRepository.delete(announcement);
}

public Collection<Announcement> findAnnouncementsForRendezvous(Integer rendezvousId){
	return announcementRepository.findAnnouncementsForRendezvous(rendezvousId);
}

@Autowired
private Validator validator;

public Announcement reconstruct(AnnouncementForm announcementForm, Integer rendezvousId, BindingResult binding){
	Announcement result;
	
	result = this.create(rendezvousId);
	
	result.setTitle(announcementForm.getTitle());
	result.setMoment(announcementForm.getMoment());
	result.setDescription(announcementForm.getDescription());
	
	validator.validate(result, binding);
	
	
	return result;
	
}



// Other business methods -------------------------------------------------
}

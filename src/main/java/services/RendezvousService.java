package services;

import domain.Rendezvous;
import domain.User;
import forms.RendezvousForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.RendezvousRepository;

import java.util.Collection;
import java.util.Date;

@Service
@Transactional
public class RendezvousService {
	// Managed repository -----------------------------------------------------
	@Autowired
	private RendezvousRepository rendezvousRepository;

	@Autowired
	private ActorService actorService;
	// Suporting repository --------------------------------------------------

	// Constructors -----------------------------------------------------------
	public RendezvousService() {
		super();
	}

	public Rendezvous create() {
		Rendezvous result;
		result = new Rendezvous();
		return result;
	}

	public Collection<Rendezvous> findAll() {
		Collection<Rendezvous> result;
		result = rendezvousRepository.findAll();
		Assert.notNull(result);
		return result;
	}

	public Rendezvous findOne(Integer rendezvousId) {
		Rendezvous result;
		Assert.notNull(rendezvousId);
		result = rendezvousRepository.findOne(rendezvousId);
		Assert.notNull(result);
		return result;
	}

	public Rendezvous save(Rendezvous rendezvous) {
		Assert.notNull(rendezvous);
		Rendezvous result;
		result = rendezvousRepository.save(rendezvous);
		Assert.notNull(result);
		return result;
	}

	public void delete(Rendezvous rendezvous) {
		assert rendezvous != null;
		assert rendezvous.getId() != 0;
		rendezvousRepository.delete(rendezvous);
	}

	public RendezvousForm construct(Rendezvous rendezvous){
		RendezvousForm res = new RendezvousForm();
		res.setRendezvousId(rendezvous.getId());
		res.setName(rendezvous.getName());
		res.setDescription(rendezvous.getDescription());
		res.setMoment(rendezvous.getMoment());
		res.setPicture(rendezvous.getPicture());
		res.setCoordinateX(rendezvous.getCoordinateX());
		res.setCoordinateY(rendezvous.getCoordinateY());
		res.setDraft(rendezvous.getDraft());
		res.setAdultflag(rendezvous.getAdultflag());
		return res;
	}

	public Rendezvous reconstruct(RendezvousForm rendezvousForm) {
		Rendezvous res;
		if(rendezvousForm.getRendezvousId() == 0){
			res = this.create();
		}else {
			res = rendezvousRepository.findOne(rendezvousForm.getRendezvousId());
		}
		Assert.isTrue(rendezvousForm.getMoment().after(new Date()), "rendezvous.message.datewrong");
		res.setAdultflag(rendezvousForm.getAdultflag());
		res.setCoordinateX(rendezvousForm.getCoordinateX());
		res.setCoordinateY(rendezvousForm.getCoordinateY());
		res.setDescription(rendezvousForm.getDescription());
		res.setMoment(rendezvousForm.getMoment());
		res.setName(rendezvousForm.getName());
		res.setPicture(rendezvousForm.getPicture());
		res.setDraft(rendezvousForm.getDraft());
		return res;
	}
	// Other business methods -------------------------------------------------

	public void isMine(Integer rendezvousId){
		Rendezvous rendezvous = this.findOne(rendezvousId);
		User user = (User) actorService.getPrincipal();
		Assert.isTrue(rendezvous.getUsers().contains(user));
	}


	public Collection<Rendezvous> findAllNot18(){
		return rendezvousRepository.findAllNot18();
	}

	public Collection<Rendezvous> findAll18(){
		return rendezvousRepository.findAll18();
	}

	public Rendezvous findOneNot18(Integer rendezvousId){
		Rendezvous res = rendezvousRepository.findOneNot18(rendezvousId);
		Assert.notNull(res);
		return res;
	}

	public Rendezvous findOne18(Integer rendezvousId){
		Rendezvous res = rendezvousRepository.findOne18(rendezvousId);
		Assert.notNull(res);
		return res;
	}

	public Collection<Rendezvous> getAll() {
		Collection<Rendezvous> res;
		User u = (User) actorService.getPrincipal();
		if (new Date().getTime() - u.getBirthday().getTime() >= 567648000000.0) {
			res = this.findAll18();
		} else {
			res = this.findAllNot18();
		}
		return res;
}

	public Rendezvous getOne(Integer rendezvousId) {
		Rendezvous res = findOne(rendezvousId);
		User u = (User) actorService.getPrincipal();
		if(!(res.getAuthor().getId()==u.getId())) {
			if (new Date().getTime() - u.getBirthday().getTime() >= 567648000000.0) {
				res = this.findOne18(rendezvousId);
			} else {
				res = this.findOneNot18(rendezvousId);
			}
		}
		return res;
	}

	public Collection<Rendezvous> findMyRendezvous(){
		User u = (User) actorService.getPrincipal();
		Collection<Rendezvous> res = rendezvousRepository.findByAuthor(u.getId());
		Assert.notNull(res);
		return res;

	}
}


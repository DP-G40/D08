package forms;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.hibernate.validator.constraints.URL;
import org.springframework.format.annotation.DateTimeFormat;

public class CommentForm {
	 // Atributos ----
    private String text;
    private String picture;
    private Integer rendezvousId;

    // Constructor ----

    public CommentForm() {
        super();
    }


    @NotNull
    @NotBlank
    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @URL
    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @NotNull
    public Integer getRendezvousId() {
        return rendezvousId;
    }

    public void setRendezvousId(Integer rendezvousId) {
        this.rendezvousId = rendezvousId;
    }
}

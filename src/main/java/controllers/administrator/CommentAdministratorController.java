package controllers.administrator;

import controllers.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.CommentService;
import services.RendezvousService;

@Controller
@RequestMapping("/comment/administrator")
public class CommentAdministratorController extends AbstractController {

    @Autowired
    private CommentService commentService;

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView delete(Integer commentId) {
        ModelAndView result;
        try {

            result = new ModelAndView(
                    "redirect:/rendezvous/administrator/view.do?rendezvousId="
                            + commentService.findOne(commentId).getRendezvous()
                            .getId());
            commentService.delete(commentService.findOne(commentId));

        } catch (Throwable oops) {
            result = new ModelAndView("redirect:/#");
        }
        return result;
    }
}
package controllers.unauthenticate;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AnnouncementService;
import services.RendezvousService;
import controllers.AbstractController;
import domain.Announcement;
import domain.Rendezvous;

@Controller
@RequestMapping("/announcement")
public class AnnouncementController extends AbstractController {

	@Autowired
	private AnnouncementService announcementService;
	
	@Autowired
	private RendezvousService rendezvousService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam Integer rendezvousId) {
		ModelAndView res = new ModelAndView("announcement/list");

		Collection<Announcement> announcements = announcementService.findAnnouncementsForRendezvous(rendezvousId);
		Rendezvous rendez = rendezvousService.findOne(rendezvousId);

		res.addObject("announcements", announcements);
		res.addObject("nombreRendezvous", rendez.getName());
	
		res.addObject("requestURI", "/announcement/list.do");

		return res;
	}

}

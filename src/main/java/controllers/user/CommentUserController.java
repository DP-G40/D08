package controllers.user;

import controllers.AbstractController;
import domain.Comment;
import forms.CommentForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.CommentService;
import services.RendezvousService;

import javax.validation.Valid;

@Controller
@RequestMapping("/comment/user")
public class CommentUserController extends AbstractController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private ActorService actorService;

    @Autowired
    private RendezvousService rendezvousService;


    @RequestMapping(value = "/create")
    public ModelAndView create(@RequestParam Integer rendezvousId) {
        ModelAndView res = new ModelAndView("comment/create");
        try {
            rendezvousService.isMine(rendezvousId);
            CommentForm commentForm = new CommentForm();
            commentForm.setRendezvousId(rendezvousId);
            res.addObject("commentForm", commentForm);
            res.addObject("action", "comment/user/create.do?rendezvousId=" + rendezvousId);
        } catch (Throwable oops) {
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid CommentForm commentForm, BindingResult result) {
        ModelAndView res = new ModelAndView("comment/create");
        if (result.hasErrors()) {
            res.addObject("commentForm", commentForm);
        } else {
            try {
                Comment comment = commentService.reconstruct(commentForm);
                commentService.save(comment);
                res = new ModelAndView("redirect:/rendezvous/user/view.do?rendezvousId="+commentForm.getRendezvousId());
            } catch (Throwable oops) {
                res.addObject("commentForm", commentForm);
                res.addObject("message", "commit.error");
            }
        }
        return res;
    }

}

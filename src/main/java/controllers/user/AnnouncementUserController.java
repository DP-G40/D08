package controllers.user;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AnnouncementService;
import controllers.AbstractController;
import domain.Announcement;
import forms.AnnouncementForm;

@Controller
@RequestMapping("/announcement/user")
public class AnnouncementUserController extends AbstractController{
	
	@Autowired
	private AnnouncementService announcementService; 
	
	
	 @RequestMapping(value = "/create")
	 public ModelAndView create(@RequestParam Integer rendezvousId) {
	        ModelAndView res = new ModelAndView("announcement/create");
	        AnnouncementForm announcementForm = new AnnouncementForm();
	        try{
	            
	            res.addObject("announcement",announcementForm);
	            res.addObject("action","announcement/user/create.do?rendezvousId="+rendezvousId);
	        }catch(Throwable oops){
	            res = new ModelAndView("redirect:/#");
	        }
	        return res;
	    }


	    @RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	    public ModelAndView save(@Valid AnnouncementForm announcementForm,@RequestParam Integer rendezvousId,BindingResult result) {
	        ModelAndView res = new ModelAndView("announcement/create");
	        Announcement announcement;
	        
	        announcement = announcementService.reconstruct(announcementForm,rendezvousId, result);
	        if (result.hasErrors()) {
	            res.addObject("announcement", announcementForm);
	            res.addObject("action", "announcement/user/create.do");
	        } else {
	            try {
	                announcementService.save(announcement);
	                //FALTA CAMBIAR ESTA DIRECCION
	                res = new ModelAndView("redirect:/announcement/user/list.do");
	            } catch (Throwable oops) {
	                res.addObject("announcement", announcement);
	                res.addObject("message", "commit.error");
	            }
	        }
	        return res;
	    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

package controllers.user;

import controllers.AbstractController;
import domain.Actor;
import domain.Comment;
import domain.Rendezvous;
import domain.User;
import forms.RendezvousForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.RendezvousService;

import javax.validation.Valid;

import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping("/rendezvous/user")
public class RendezvousUserController extends AbstractController {
	@Autowired
	private RendezvousService rendezvousService;

	@Autowired
	private ActorService actorService;

	@RequestMapping(value = "/rsvp", method = RequestMethod.GET)
	public ModelAndView rsvp(@RequestParam Integer rendezvousId) {
		ModelAndView res = new ModelAndView("redirect:list.do");
		try {
			User user = (User) actorService.getPrincipal();
			Rendezvous rendezvous = rendezvousService.findOne(rendezvousId);
			if (!rendezvous.getUsers().contains(user)) {
				rendezvous.getUsers().add(user);
				rendezvousService.save(rendezvous);
			}
		} catch (Throwable oops) {
			res = new ModelAndView("redirect:/#");
		}
		return res;
	}

	@RequestMapping(value = "/cancel", method = RequestMethod.GET)
	public ModelAndView cancel(@RequestParam Integer rendezvousId) {
		ModelAndView res = new ModelAndView("redirect:list.do");
		try {
			User user = (User) actorService.getPrincipal();
			Rendezvous rendezvous = rendezvousService.findOne(rendezvousId);
			if (rendezvous.getUsers().contains(user)) {
				rendezvous.getUsers().remove(user);
				rendezvousService.save(rendezvous);
			}
		} catch (Throwable oops) {
			res = new ModelAndView("redirect:/#");
		}
		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res = new ModelAndView("rendezvous/create");
		RendezvousForm rendezvousForm = new RendezvousForm();
		res.addObject("action", "rendezvous/user/create.do");
		res.addObject("rendezvousForm", rendezvousForm);
		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid RendezvousForm rendezvousForm, BindingResult binding) {
		ModelAndView res = new ModelAndView("rendezvous/create");
		if (binding.hasErrors()) {
			res.addObject("action", "rendezvous/user/create.do");
			res.addObject("rendezvousForm", rendezvousForm);
		} else {
			try {
				User u = (User) actorService.getPrincipal();
				Rendezvous f = rendezvousService.reconstruct(rendezvousForm);
				f.setAuthor(u);
				f.getUsers().add(u);
				f.setDraft(false);
				f = rendezvousService.save(f);
				res = new ModelAndView("redirect:/rendezvous/user/view.do?rendezvousId=" + f.getId());

			} catch (Throwable oops) {
				res.addObject("action", "rendezvous/user/create.do");
				res.addObject("rendezvousForm", rendezvousForm);
				res.addObject("message", "actor.commit.error");

			}

		}
		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "draft")
	public ModelAndView saveDraft(@Valid RendezvousForm rendezvousForm, BindingResult binding) {
		ModelAndView res = new ModelAndView("rendezvous/create");
		if (binding.hasErrors()) {
			res.addObject("action", "rendezvous/user/create.do");
			res.addObject("rendezvousForm", rendezvousForm);
		} else {
			try {
				User u = (User) actorService.getPrincipal();
				Rendezvous f = rendezvousService.reconstruct(rendezvousForm);
				f.setAuthor(u);
				f.getUsers().add(u);
				f.setDraft(true);
				f = rendezvousService.save(f);
				res = new ModelAndView("redirect:/rendezvous/user/view.do?rendezvousId=" + f.getId());

			} catch (Throwable oops) {
				res.addObject("action", "rendezvous/user/create.do");
				res.addObject("rendezvousForm", rendezvousForm);
				res.addObject("message", "actor.commit.error");

			}

		}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int rendezvousId) {
		ModelAndView res = new ModelAndView("rendezvous/edit");
		Rendezvous aux = rendezvousService.findOne(rendezvousId);
		Actor principal = actorService.getPrincipal();

		if (aux.getDraft() == false || aux.getBanned() == true || aux.getAuthor().getId() != principal.getId()) {
			res = new ModelAndView("redirect:/error/sorry.do");
		} else {
			RendezvousForm rendezvousForm = rendezvousService.construct(aux);
			res.addObject("action", "rendezvous/user/edit.do");
			res.addObject("rendezvousForm", rendezvousForm);
			res.addObject("principal", principal);
		}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEditPublish(@Valid RendezvousForm rendezvousForm, BindingResult binding) {
		ModelAndView res = new ModelAndView("rendezvous/edit");
		if (binding.hasErrors()) {
			res.addObject("action", "rendezvous/user/edit.do");
			res.addObject("rendezvousForm", rendezvousForm);
		} else {
			try {
				User u = (User) actorService.getPrincipal();
				Rendezvous f = rendezvousService.reconstruct(rendezvousForm);
				f.setAuthor(u);
				f.setDraft(false);
				f = rendezvousService.save(f);
				res = new ModelAndView("redirect:/rendezvous/user/view.do?rendezvousId=" + f.getId());

			} catch (Throwable oops) {
				res.addObject("action", "rendezvous/user/edit.do");
				res.addObject("rendezvousForm", rendezvousForm);
				res.addObject("message", "actor.commit.error");

			}
		}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "draft")
	public ModelAndView saveEditDraft(@Valid RendezvousForm rendezvousForm, BindingResult binding) {
		ModelAndView res = new ModelAndView("rendezvous/edit");
		if (binding.hasErrors()) {
			res.addObject("action", "rendezvous/user/edit.do");
			res.addObject("rendezvousForm", rendezvousForm);
		} else {
			try {
				User u = (User) actorService.getPrincipal();
				Rendezvous f = rendezvousService.reconstruct(rendezvousForm);
				f.setAuthor(u);
				f.setDraft(true);
				f = rendezvousService.save(f);
				res = new ModelAndView("redirect:/rendezvous/user/view.do?rendezvousId=" + f.getId());

			} catch (Throwable oops) {
				res.addObject("action", "rendezvous/user/edit.do");
				res.addObject("rendezvousForm", rendezvousForm);
				res.addObject("message", "actor.commit.error");

			}
		}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView deleteEditDraft(@Valid RendezvousForm rendezvousForm, BindingResult binding) {
		ModelAndView res = new ModelAndView("rendezvous/edit");
		if (binding.hasErrors()) {
			res.addObject("action", "rendezvous/user/edit.do");
			res.addObject("rendezvousForm", rendezvousForm);
		} else {
			try {
				User u = (User) actorService.getPrincipal();
				Rendezvous f = rendezvousService.reconstruct(rendezvousForm);
				f.setAuthor(u);
				f.setActive(false);
				f = rendezvousService.save(f);
				res = new ModelAndView("redirect:/rendezvous/list.do");

			} catch (Throwable oops) {
				res.addObject("action", "rendezvous/user/edit.do");
				res.addObject("rendezvousForm", rendezvousForm);
				res.addObject("message", "actor.commit.error");

			}
		}
		return res;
	}



	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		try {
			Collection<Rendezvous> rendezvous = rendezvousService.getAll();
			result = new ModelAndView("rendezvous/list");
			result.addObject("rendezvous", rendezvous);
			result.addObject("requestURI", "/rendezvous/list.do");
			result.addObject("actor",actorService.getPrincipal());
		}catch (Throwable oops){
			result = new ModelAndView("redirect:/error/sorry.do");
		}
		return result;
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(@RequestParam Integer rendezvousId) {
		ModelAndView result;
		try {
			result = new ModelAndView("rendezvous/view");
			Rendezvous rendezvous = rendezvousService.getOne(rendezvousId);
			result.addObject("rendezvous", rendezvous);

			Collection<User> userreg = rendezvous.getUsers();
			result.addObject("usersapuntados", userreg);
			Collection<Comment> comments = rendezvous.getComments();
			result.addObject("comments", comments);
			result.addObject("actor",actorService.getPrincipal());

		}catch (Throwable oops){
			result = new ModelAndView("redirect:/error/sorry.do");
		}
		return result;
	}

	@RequestMapping(value = "/myrendezvous", method = RequestMethod.GET)
	public ModelAndView myRendezvous() {
		ModelAndView result;
		try {
			Collection<Rendezvous> rendezvous = rendezvousService.findMyRendezvous();
			result = new ModelAndView("rendezvous/myrendezvous");
			result.addObject("rendezvous", rendezvous);
			result.addObject("requestURI", "/rendezvous/myrendezvous.do");
		}catch (Throwable oops){
			result = new ModelAndView("redirect:/error/sorry.do");
		}
		return result;
	}

}





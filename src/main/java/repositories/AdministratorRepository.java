package repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.Administrator;
import domain.Rendezvous;
@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {

	//C
		@Query("select avg(u.rendezvous.size),stddev(u.rendezvous.size) from User u")
		Object Q1();
		
		@Query("select sum(case when u.rendezvous.size > 0 then 1.00 else 0.00 end)/sum(case when u.rendezvous.size = 0 then 1.00 else 0.00 end) from User u")
		Double Q2();
		
		@Query("select avg(r.users.size),stddev(r.users.size) from Rendezvous r")
		Object Q3();
		
		@Query("select avg(u.rendezvous.size),stddev(u.rendezvous.size) from User u join u.rendezvous r where r.users.size >= 1")
		Object Q4();
		
		@Query("select r from Rendezvous r order by r.users.size")
		List<Rendezvous> Q5(Pageable pageable);
		
		//B
		
		@Query("select avg(r.announcements.size),stddev(r.announcements.size) from Rendezvous r")
		Object Q6();
		
		@Query("select r from Rendezvous r where r.announcements.size >= ALL(select avg(r2.announcements.size)*0.75 from Rendezvous r2)")
		List<Rendezvous> Q7();
		
		@Query("select r from Rendezvous r where r.rendezvous.size >= ALL(select avg(r2.rendezvous.size)*1.10 from Rendezvous r2)")
		List<Rendezvous> Q8();

}

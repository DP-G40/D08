package repositories;
import java.util.Collection;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.Announcement;
@Repository
public interface AnnouncementRepository extends JpaRepository<Announcement, Integer> {
	
	@Query("select a from Announcement a where a.rendezvous.id = ?1")
	public Collection<Announcement> findAnnouncementsForRendezvous(Integer rendezvousId);


}

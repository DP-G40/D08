package repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.Rendezvous;

import java.util.Collection;

@Repository
public interface RendezvousRepository extends JpaRepository<Rendezvous, Integer> {

    @Query("select r from Rendezvous r where r.active=true and r.draft=false and r.banned = false and r.adultflag = false")
    Collection<Rendezvous> findAllNot18();

    @Query("select r from Rendezvous r where r.active=true and r.draft=false and r.banned = false")
    Collection<Rendezvous> findAll18();

    @Query("select r from Rendezvous r where r.active=true and r.draft=false and r.banned = false and r.adultflag = false and r.id = ?1")
    Rendezvous findOneNot18(Integer rendezvousId);

    @Query("select r from Rendezvous r where r.active=true and r.draft=false and r.banned = false  and r.id = ?1")
    Rendezvous findOne18(Integer rendezvousId);

    @Query("select r from Rendezvous r where  r.author.id = ?1")
    Collection<Rendezvous> findByAuthor(Integer authorId);



}

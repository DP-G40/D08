<%@page language="java" contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="${action}" modelAttribute="actorform" method="POST">

	<acme:textbox code="row.name" path="name"/>
	<acme:textbox code="row.surname" path="surname"/>
	<acme:textbox code="row.email" path="email"/>
	<acme:textbox code="row.phone" path="phoneNumber"/>
	<acme:textbox code="row.direction;" path="direction;"/>

<tr>
			<td colspan="3">
				<button type="submit" name="save" class="btn btn-primary"
						onclick="return validateFeedback()">
					<spring:message code="explorer.save" />
				</button>&nbsp;
				<acme:cancel code="actor.cancel" url="/#" />
			</td>
		</tr>

</form:form>

<script type="text/javascript">
    function validateFeedback() {
        var phone = document.getElementById("phoneNumber");
        var RE = /(\+\d{1,3} \d{1,3} \d{4,}$) |(\+\d{1,3} \d{4,}$) |(\d{4,}$)/;
        if (!(phone.value).match(RE)) {
            alert('<spring:message code="row.confirm.register" />');
        }
        return confirm('<spring:message code="row.confirm.edit" />');
    }
</script>
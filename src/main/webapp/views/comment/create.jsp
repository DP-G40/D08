<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="${action}" modelAttribute="commentForm">

	<form:hidden path="rendezvousId" />

	<acme:textbox code="comment.text" path="text" />
	<acme:textbox code="comment.picture" path="picture" />
	

	<tr>
		<td colspan="3">
		<acme:submit code="comment.save" name="save" /> 
		<acme:cancel code="comment.cancel" url="/comment/user/list.do" />
		
		</td>
	</tr>

</form:form>

<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %><%--
 * action-1.jsp
 *
 * Copyright (C) 2013 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%
	Date date = new Date();
	SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");
	String now = sp.format(date);
	pageContext.setAttribute("now",now);
%>

<form:form action="user/register.do" modelAttribute="registerForm" method="post">

	<acme:textbox code="user.name" path="name" />
	<acme:textbox code="user.surname" path="surname" />
	<acme:textbox code="user.email" path="email" />
	<acme:textbox code="user.phone" path="phone" />
	<acme:textbox code="user.address" path="address" />
	<acme:textbox code="user.birthday" path="birthday" type="date" max="${now}"/>
	<br />
	<acme:textbox code="user.userName" path="userName" />
	<acme:password code="user.password" path="password" />
	<acme:password code="user.repeatPassword" path="repeatPassword" />
	<acme:checkbox code="user.termsAndConditions" path="check" />
	<br />

	<br />
	
	<acme:submit name="save" code="user.save"/>
	<acme:cancel code="user.cancel" url="" />

</form:form>
<%@ page import="com.lowagie.tools.plugins.AbstractTool.Console"%>
<%@ page import="java.util.Date" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<security:authentication property="principal" var="principal"/>


    <h2><spring:message code="user.view"/></h2>
    <display:table name="user" id="row" requestURI="${requestURI}">
    
        <display:column property="name" titleKey="user.name" sortable="true"/>
        <display:column property="surname" titleKey="user.surname"/>
        <display:column property="email" titleKey="user.email" sortable="false"/>
        <display:column property="phoneNumber" titleKey="user.phoneNumber" sortable="false"/>
        <display:column property="direction" titleKey="user.direction" sortable="false"/>
        <display:column property="birthday" titleKey="user.birthday" sortable="false" format="{0,date,dd/MM/yyyy}"/>

    </display:table>
    
    
    
    <h2><spring:message code="user.rendezvouses"/></h2>
    <display:table name="rendezvouses" id="row" requestURI="${requestURI}">
    
        <display:column property="name" titleKey="rendezvous.name" sortable="true"/>
        <display:column property="moment" titleKey="rendezvous.moment" sortable="false" format="{0,date,dd/MM/yyyy}"/>
        <security:authorize access="isAnonymous()">
            <display:column titleKey="user.view" >
                <acme:cancel code="user.view" url="rendezvous/view.do?rendezvousId=${row.id}"/>
            </display:column>
        </security:authorize>

        <security:authorize access="hasRole('USER')">
            <display:column titleKey="user.view" >
                <acme:cancel code="user.view" url="rendezvous/user/view.do?rendezvousId=${row.id}"/>
            </display:column>
        </security:authorize>

        <security:authorize access="hasRole('ADMIN')">
            <display:column titleKey="user.view">
                <acme:cancel code="user.view" url="rendezvous/administrator/view.do?rendezvousId=${row.id}"/>
            </display:column>
        </security:authorize>


    </display:table>

    
    
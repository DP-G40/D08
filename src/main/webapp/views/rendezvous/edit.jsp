<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%
    Date date = new Date();
    SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");
    String now = sp.format(date);
    pageContext.setAttribute("now",now);
%>

<form:form action="${action}" modelAttribute="rendezvousForm" method="post">

    <form:hidden path="rendezvousId" />

    <acme:textbox code="rendezvous.name" path="name" />
    <acme:textbox code="rendezvous.description" path="description" />
    <acme:textbox code="rendezvous.moment" path="moment" type="date" min="${now}"/>

    <br />
    <acme:textbox code="rendezvous.picture" path="picture" />
    <acme:textbox code="rendezvous.coordinateX" path="coordinateX" />
    <acme:textbox code="rendezvous.coordinateY" path="coordinateY" />
    <br />
    <acme:checkbox path="adultflag" code="rendezvous.adultflag"/>

    <acme:submit name="save" code="rendezvous.publish"/>

    <jstl:if test="${rendezvousForm.draft==true || rendezvousForm.draft==null}">
        <acme:submit name="draft" code="rendezvous.saveAsDraft"/>
        <jstl:if test="${rendezvousForm.rendezvousId !=0 }" >
        <acme:submit name="delete" code="standard.delete"/>
        </jstl:if>
    </jstl:if>



    <jstl:if test="${rendezvousForm.rendezvousId!=0}">
    <acme:cancel code="user.cancel" url="/rendezvous/user/view.do?rendezvousId=${rendezvousForm.rendezvousId}" />
    </jstl:if>
    <jstl:if test="${rendezvousForm.rendezvousId == 0}">
        <acme:cancel code="user.cancel" url="/#" />
    </jstl:if>

</form:form>